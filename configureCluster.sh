#!/bin/bash
KUBE_CA_PEM_FILE=$1
KUBE_URL=$2 
KUBE_TOKEN=$3
kubectl config set-cluster ci-demo --server="$KUBE_URL" --certificate-authority="$KUBE_CA_PEM_FILE"
kubectl config set-credentials ci-demo --token="$KUBE_TOKEN" --certificate-authority="$KUBE_CA_PEM_FILE"
kubectl config set-context ci-demo --cluster=ci-demo --user=ci-demo
kubectl config use-context ci-demo
kubectl version