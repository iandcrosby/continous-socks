FROM golang:1.9-alpine

COPY *.go /go/
RUN go build -o /deals

CMD /deals
EXPOSE 8080